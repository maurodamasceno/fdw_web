// 04_Conceitos_Ex20 forms.js


function verificarFormulario() {
  var mensagemErro = "";
  if (document.getElementById("email").value == "") {
    mensagemErro = "Informe o email";
  }
  if (document.getElementById("login").value == "") {
    mensagemErro += "\nInforme o login";
  }
  if (document.getElementById("datanasc").value == "") {
    mensagemErro += "\nInforme a Data de Nascimento";
  }
  if (mensagemErro == "") {
    return true;
  } else {
    alert(mensagemErro);
    return false;
  }
}

function mascData(objeto, evento) {
  if (evento.keyCode == 8 || evento.keyCode == 46) {
    return true;
  }
  if (objeto.value.length == 10) {
    return false;
  }
  if (evento.keyCode < 48 || evento.keyCode > 57) {
    alert("Somente números");
    return false;
  }
  if ((objeto.value.length == 2) || (objeto.value.length == 5)) {
    objeto.value = objeto.value + '/';
  }
  return true;
}

// VALIDANDO TODOS OS CAMPOS DE UMA VEZ
function verificarFormulario() {
  var mensagemErro = "";
  if (document.getElementById("email").value == "") {
    mensagemErro = "Informe o email";
  }
  if (document.getElementById("login").value == "") {
    mensagemErro += "\nInforme o login";
  }
  if (document.getElementById("datanasc").value == "") {
    mensagemErro += "\nInforme a Data de Nascimento";
  }
  if (document.getElementById("escolaridade").value == "") {
    mensagemErro += "\nInforme a Escolaridade";
  }
  var opcoesSexo = document.getElementsByName("sexo");
  var sexoPreenchido = false;
  for (var i = 0; i < opcoesSexo.length; i++) {
    if (opcoesSexo[i].checked) {
      sexoPreenchido = true;
    }
  }
  if (!sexoPreenchido) {
    mensagemErro += "\nInforme o sexo";
  }
  if (mensagemErro == "") {
    return true;
  } else {
    alert(mensagemErro);
    return false;
  }
}