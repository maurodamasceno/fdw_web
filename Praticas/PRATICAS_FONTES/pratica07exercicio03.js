/* pratica07exercicio03.html, pratica07exercicio03.css, pratica07exercicio03.js. 
Ao passar o mouse sobre alguma das linhas ou colunas da tabela, deve ser apresentada
uma formatação diferente para a linha ou coluna, conforme apresentado abaixo. Ao clicar em

alguma das linhas da tabela, deve ser apresentado uma mensagem informando se a nota total
do aluno está acima ou abaixo da média da disciplina relacionada à linha clicada. 
A mensagem ainda deve apresentar o resultado da subtração entre a nota total do aluno e a média da
disciplina.
*/

function mostraMensagem(alunoNome, notaTotalAluno, disciplina, mediaTurmaDisciplina) {

  let mensagem = "";
  let situacao = "";

  let diferenca = parseFloat(notaTotalAluno) - parseFloat(mediaTurmaDisciplina);

  if (diferenca > 0)
    situacao = "acima";
  else if (diferenca < 0)
    situacao = "abaixo";
  else
    situacao = "empatada";

  mensagem += "A nota total do aluno (" + alunoNome + ") é (" + parseFloat(notaTotalAluno).toFixed(2) + ") \n";
  mensagem += "e está " + situacao + " em relacao à média da turma na disciplina ";
  mensagem += disciplina + " (" + parseFloat(mediaTurmaDisciplina).toFixed(2) + ") \n";
  mensagem += "A diferenca foi de (" + diferenca.toFixed(2) + ")";

  alert(mensagem);
}
