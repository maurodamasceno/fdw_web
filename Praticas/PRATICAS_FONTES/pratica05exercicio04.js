// Crie os arquivos pratica05exercicio04.html, pratica05exercicio04.css e pratica05exercicio04.js.
// 4) Usando HTML, CSS e JS, escreva um algoritmo que leia um número maior ou igual a zero a partir
// do teclado.O algoritmo deve apresentar no navegador todos os números pares maiores ou
// iguais a zero e menores ou iguais ao número informado.

function calculaPares() {

  var teto = document.getElementById("numeroInformado").value;
  var resposta = "Pares entre (zero) e (" + teto + "): ";

  if (teto == "") {
    alert("Você esqueceu de informar o número?");
    document.getElementById("numeroInformado").focus();
  }
  else {
    teto = parseInt(teto);
  }

  if (teto <= 0) {
    resposta = "Número deve ser maior ou igual a zero."
  }
  else {
    for (i = 2; i <= teto; i += 2) {

      resposta += i + " : ";
    }
  }

  document.getElementById("resultado").innerHTML = resposta;

}