// Crie os arquivos pratica05exercicio03.html, pratica05exercicio03.css e pratica05exercicio03.js.
// 3) Usando HTML, CSS e JS, escreva um algoritmo que leia um número a partir do teclado.O
// algoritmo deve informar se o número lido é par ou ímpar.

function parOuImpar() {

  var resposta = "";
  var numero = document.getElementById("numeroInput").value;

  if (numero == "") {
    alert("Você esqueceu de informar o número?");
    document.getElementById("numeroInput").focus();
  }
  else {
    numero = parseInt(numero);
  }

  if (numero == 0) {
    resposta = "Zero não é par nem ímpar.";
  }
  else if (numero % 2 == 0) {
    resposta = "O número " + numero + " é par.";
  }
  else {
    resposta = "O número " + numero + " é ímpar.";
  }

  document.getElementById("resultadoHtm").innerHTML = resposta;

}
