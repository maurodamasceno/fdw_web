// Crie os arquivos pratica05exercicio05.html, pratica05exercicio05.css e pratica05exercicio05.js.
// 5) Usando HTML, CSS e JS, escreva um algoritmo que leia dois números positivos e maiores que
// zero a partir do teclado.O algoritmo deve apresentar como resposta o logaritmo do primeiro
// número na base representada pelo segundo número.
//   OBS: Usar: Math.log() log base b(x) = log base a(x) / log base a(b)
// a base tem que ser diferente de 1, além de maior que zero.

function calculoLog() {

  // logaritmando a
  var a = document.getElementById("logaritimando").value;
  // base b
  var b = document.getElementById("base").value;
  var x = 0;
  var resposta = "";

  if (a == "" || b == "") {
    alert("Você esqueceu de informar algum número?");
    document.getElementById("logaritimando").focus();
  }
  else {
    a = parseInt(a);
    b = parseInt(b);

    if (a <= 0 || b <= 0 || b == 1) {
      resposta = " 'a' e 'b' têm que ser positivos. A base 'b' não pode ser 1."
    }
    else {
      // Calculando pela regra da mudança de base:
      // log base b(x) = log base a(x) / log base a(b)
      x = Math.log(a) / Math.log(b);

      resposta = "O logarítmo de " + a + " na base " + b + " = " + x;

    }
    document.getElementById("resultado").innerHTML = resposta;
  }
}