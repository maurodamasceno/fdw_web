// Crie os arquivos pratica05exercicio06.html, pratica05exercicio06.css e pratica05exercicio06.js.
// 6) Usando HTML, CSS e JS, escreva um algoritmo que leia o código de uma operação a partir o
// teclado e que realize a operação associada a esse código.O código é um valor inteiro.Os códigos
// e operações são listados abaixo:
// Código Operação
// 1 Área de um retângulo.Os valores da base e da altura do retângulo devem ser
// informados pelo usuário.OBS: aretângulo = base * altura.
// 2 Área de um triângulo.Os valores da base e da altura do triângulo devem ser
// informados pelo usuário.OBS: atriângulo = (base * altura) / 2.
// 3 Área de um círculo.O valor do raio do círculo deve ser informado pelo usuário.OBS:
// acírculo = 3, 14 * (raio)2.
// 4 Área de um trapézio.Os valores da base maior, da base menor e da altura do
//   trapézio devem ser informados pelo usuário.OBS: atrapézio = [(base maior + base menor) * altura] / 2.

function areaDaFigura() {

  var figura = "";
  var area = 0;

  var opcao = parseInt(document.getElementById("opcao").value);

  switch (opcao) {
    case 1:
      // opção 1 retangulo solicitar lado A e lado B
      figura = "Retângulo";
      var ladoA = parseInt(prompt("Opção 1 [" + figura + "] : Medida do lado A: "));
      var ladoB = parseInt(prompt("Opção 1 [" + figura + "] : Medida do lado B: "));
      area = ladoA * ladoB;
      break;
    case 2:
      // opção 2 triangulo solicitar base b e altura a
      figura = "Triângulo";
      var baseTri = parseInt(prompt("Opção 2 [" + figura + "] : Medida da base : "));
      var alturaTri = parseInt(prompt("Opção 2 [" + figura + "] : Medida da altura : "));
      area = baseTri * alturaTri / 2;
      break;
    case 3:
      // opção 3 circulo solicitar raio
      figura = "Círculo";
      var raio = parseInt(prompt("Opção 3 [" + figura + "] : Medida do raio : "));
      area = raio * raio * Math.PI;
      break;
    case 4:
      // opção 4 trapezio solicitar bmaior, bmenor e altura a
      figura = "Trapézio";
      var baseMaior = parseInt(prompt("Opção 4 [" + figura + "] : Medida da baseMaior : "));
      var baseMenor = parseInt(prompt("Opção 4 [" + figura + "] : Medida do baseMenor : "));
      var alturaTra = parseInt(prompt("Opção 4 [" + figura + "] : Medida da altura: "));
      area = (baseMaior + baseMenor) * alturaTra / 2;
      break;
    default:
      document.getElementById("erro").innerHTML = "Opção inválida. (F5) para tentar de novo..."
  }
  resposta = "Figura escolhida: " + figura + " : Área calculada: " + area;
  responder();
}

function responder() {
  document.getElementById("resultado").innerHTML = resposta;
}