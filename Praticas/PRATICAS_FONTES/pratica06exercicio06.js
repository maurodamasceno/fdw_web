
// Usando HTML, CSS e JS, escreva um algoritmo que possua uma função para calcular o volume de
// uma esfera.O valor do raio da esfera deve ser informado pelo usuário a partir do teclado.O
// volume da esfera deve ser apresentado na tela do navegador.Crie os arquivos
// pratica06exercicio06.html, pratica06exercicio06.css e pratica06exercicio06.js, contendo a
// estrutura, formatação e funcionalidade da página gerada.
//   OBS: volumeesfera = (4 * 3, 14 * raio3) / 3.
function volumeEsfera() {
  var raio = parseInt(document.getElementById("raio").value);
  var volume = (Math.pow(raio, 3) * Math.PI * 4) / 3;
  document.getElementById("resposta").innerHTML = volume;
}