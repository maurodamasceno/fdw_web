/* pratica07exercicio01.html, pratica07exercicio01.css, pratica07exercicio01.js. 
Ao clicar em alguma das linhas da tabela, deve ser apresentada uma mensagem 
informando se o aluno foi aprovado ou reprovado. 
Para ser aprovado, o aluno deve ter nota total maior ou igual a 70 pontos 
e menos de 3 faltas.
*/
function informaResultado(notaTotal, faltas) {

  if (notaTotal >= 70 && faltas < 3) {
    resultado = "Aprovado.";
  } else {
    resultado = "Reprovado.";
  }
  alert("O aluno foi " + resultado);
}

